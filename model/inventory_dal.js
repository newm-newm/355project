var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAllProducts = function(callback) {
    var query = 'SELECT * FROM product;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO product(nibgrade, price, nibmaterial, color_id, manufacturer, productname) VALUES (?)';

    var queryData = [
        params.nibgrade,
        params.price,
        params.nibmaterial,
        params.color_id,
        params.manufacturer,
        params.productname
    ];
    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(product_id, callback) {
    var query = 'DELETE FROM product WHERE product_id = ' + product_id + ';';

    console.log(query);

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
