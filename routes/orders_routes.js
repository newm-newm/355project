var express = require('express');
var router = express.Router();
var orders_dal = require('../model/orders_dal');

router.get('/ViewAll', function(req, res) {
    orders_dal.getEverything(function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('orders/ordersViewAll', {title: 'Fake Fountain Pen WebStore', everything: result});
        }
    })
});

router.get('/delete', function(req, res){
    if(req.query.order_id == null) {
        res.send('order_id is null');
    }
    else {
        orders_dal.delete(req.query.order_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/orders/ordersDeleted');
            }
        });
    }
});


router.get('/ordersDeleted', function(req, res) {
    res.render('orders/ordersDeleted', { title: 'Fake Fountain Pen WebStore' });
});


// Return the add a new company form

//This is probably bad form but had no idea how to do it otherwise...
router.get('/createOrder', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    orders_dal.getAllProduct(function(err,resultProduct) {
        if (err) {
            res.send(err);
        }
        else {
            orders_dal.getAllAddress(function(err, resultAddress) {
                if(err) {
                    res.send(err);
                }
                else {
                    orders_dal.getAllUsers(function(err, resultUsers) {
                        if(err) {
                            res.send(err);
                        }
                        else {
                            res.render('orders/ordersAdd', {'product': resultProduct, 'address': resultAddress, 'users': resultUsers});
                        }

                    })
                }
            })
        }
    });
});


// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.product_id == null) {
        res.send('Product must be selected.');
    }
    else if(req.query.address_id == null) {
        res.send('Address must be selected');
    }
    else if(req.query.user_id == null) {
        res.send('User must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        orders_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/orders/ordersSubmitted');
            }
        });
    }
});

router.get('/ordersSubmitted', function(req, res) {
    res.render('orders/ordersSubmitted', { title: 'Fake Fountain Pen WebStore' });
});

module.exports = router;
