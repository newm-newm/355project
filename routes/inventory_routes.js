var express = require('express');
var router = express.Router();
var inventory_dal = require('../model/inventory_dal');

router.get('/all', function(req, res) {
    inventory_dal.getAllProducts(function(err, productResult) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('inventory/inventoryViewAll', {title: 'Fake Fountain Pen WebStore', subtitle: 'Inventory', products: productResult});
        }
    })
});

router.get('/createProduct', function(req, res) {
    res.render('inventory/inventoryAdd', {title: 'Fake Fountain Pen WebStore'});
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.nibgrade == null) {
        res.send('Nibgrade must be selected');
    }
    else if(req.query.price == null) {
        res.send('Price must be entered');
    }
    else if(req.query.nibmaterial == null) {
        res.send('Nib Material must be selected');
    }
    else if(req.query.manufacturer == null) {
        res.send('Manufacturer must be selected');
    }
    else if(req.query.productname == null) {
        res.send('Product Name must be entered');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        inventory_dal.insert(req.query, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/inventory/all');
            }
        });
    }
});

router.get('/inventoryDeleted', function(req, res) {
    res.render('inventory/inventoryDeleted', { title: 'Fake Fountain Pen WebStore' });
});


router.get('/delete', function(req, res){
    if(req.query.product_id == null) {
        res.send('product_id is null');
    }
    else {
        inventory_dal.delete(req.query.product_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/inventory/inventoryDeleted');
            }
        });
    }
});


// Return the add a new company form

//This is probably bad form but had no idea how to do it otherwise...
router.get('/createOrder', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    orders_dal.getAllProduct(function(err,resultProduct) {
        if (err) {
            res.send(err);
        }
        else {
            orders_dal.getAllAddress(function(err, resultAddress) {
                if(err) {
                    res.send(err);
                }
                else {
                    orders_dal.getAllUsers(function(err, resultUsers) {
                        if(err) {
                            res.send(err);
                        }
                        else {
                            res.render('orders/ordersAdd', {'product': resultProduct, 'address': resultAddress, 'users': resultUsers});
                        }

                    })
                }
            })
        }
    });
});


// View the company for the given id

router.get('/inventorySubmitted', function(req, res) {
    res.render('inventory/inventorySubmitted', { title: 'Fake Fountain Pen WebStore' });
});

module.exports = router;
